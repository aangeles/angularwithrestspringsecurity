var myApp = angular.module('myApp', []);

myApp.controller('myCtrl', function($scope, $http) {

	$scope.showLogin = true;
	
	$scope.user = { name: "", password: ""};
	$scope.greeting = {id:'...', content:'...'};
	$scope.txt = "";

	$scope.login = function() {

		var headers = { authorization : "Basic " + btoa( $scope.user.name + ':' + $scope.user.password)  } ;

		$http.get('/', {headers : headers}).		
		success(function(data) {
			$scope.showLogin = false;
		}).error(function(data, status){
			alert("login error");
		});
	}
	
	$scope.logout = function() {
		$http.get('http://localhost:8080/logout').
		success(function(data) {
			$scope.showLogin = true;
		}).error(function(data, status){
			alert("logout error");
		});
		
	}

	$scope.call = function (value) {		
		$http.get('http://localhost:8080/' + value + "?name=" + $scope.txt).
		success(function(data) {
			$scope.greeting = data;
		}).error(function(data, status){
				alert("Status: " + status);
		});
	}
})

