package aar.example;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
        	.inMemoryAuthentication()
	        	.withUser("admin").password("admin").roles("ADMIN","USER")
	        	.and()
	        	.withUser("user").password("user").roles("USER");
    }
	
	
	@Override
	  protected void configure(HttpSecurity http) throws Exception {
		http
			.csrf().disable()
			.httpBasic()
			.and()			
			.authorizeRequests()
				.antMatchers("/").permitAll()
				.antMatchers("/greeting/**").permitAll()
				.antMatchers("/secureGreeting/**").authenticated()
				.antMatchers("/adminGreeting/**").hasAnyRole("ADMIN")
				.antMatchers("/principalGreeting/**").authenticated()
			.and()
        	.logout()
        		.logoutUrl("/logout")
        		.logoutSuccessUrl("/")
        		.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
        		.invalidateHttpSession(true);
        		
        

	  }
}