package aar.example;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PrincipalGreetingController {

    private static final String template = "Hello, %s! (Principal)";
    private final AtomicLong counter = new AtomicLong();

    @PreAuthorize("#name == authentication.name")
    @RequestMapping("/principalGreeting")
    public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
        return new Greeting(counter.incrementAndGet(),
                            String.format(template, name));
    }
}