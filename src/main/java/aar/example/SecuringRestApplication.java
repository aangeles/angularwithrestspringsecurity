package aar.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecuringRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecuringRestApplication.class, args);
	}
}
