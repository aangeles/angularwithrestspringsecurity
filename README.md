# Angular With REST Spring Security
This example show you how to connect a Angular application with REST services developed under Spring Boot and Spring Security using Basic Credential.

You will find the following REST services:
  - Unsecured greeting
  - Secured greeting
  - Admin greeting: just for the user Admin
  - Principal greeting: require to recibe as an argument the username of the authenticated user

With the angular application you can call all the services with o without be authentication.

# Getting Starting

```sh
$ git clone https://bitbucket.org/aangeles/angularwithrestspringsecurity.git
$ cd angularwithrestspringsecurity
$ mvn spring-boot:run
```
After this step the example will be running at http://localhost:8080

License
----

MIT
